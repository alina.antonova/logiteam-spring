package org.alina.agileengine.logiteam.controller;

import org.alina.agileengine.logiteam.data.Project;
import org.alina.agileengine.logiteam.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/list")
    public List<Project> getProjects() {
        return projectService.getProjectlist();
    }

    @GetMapping("{id}")
    public Project getProject(@PathVariable long id) {
        return projectService.getProject(id);
    }

    @PostMapping("/addProject")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Project addProject(@Valid @RequestBody Project project) {
        return projectService.addProject(project);
    }

    @DeleteMapping("{id}")
    public  @ResponseBody
    ResponseEntity deleteProject (@PathVariable long id) {
        boolean result = projectService.removeProject(id);
        if (!result){
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().build();
        }

    }

    @GetMapping("")
    public List<Project> getProjectByName(@RequestParam("name") String name) {
        return projectService.getProjectByName(name);
    }

}
