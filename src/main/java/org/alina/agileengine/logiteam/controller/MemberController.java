package org.alina.agileengine.logiteam.controller;

import java.io.IOException;
import java.util.List;
import org.alina.agileengine.logiteam.data.Member;
import org.alina.agileengine.logiteam.services.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    //Get list of members
    @GetMapping("/list")
    public List<Member> getMembers() {
        return memberService.getMembers();
    }

    //Get member by id
    @GetMapping("{id}")
    public Member getMember(@PathVariable long id) {
        return memberService.getMember(id);
    }

    @PostMapping("/addmember")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Member addMember(@Valid @RequestBody Member member) throws IOException {
        return memberService.addMember(member);
    }

//    @DeleteMapping("{id}")
//    public  @ResponseBody ResponseEntity deleteMember (@PathVariable long id) {
//        boolean result = memberService.removeMember(id);
//        if (!result){
//            return ResponseEntity.notFound().build();
//        } else {
//            return ResponseEntity.ok().build();
//        }
//
//    }

    @GetMapping
    public List<Member> getMemberByRole(@RequestParam("role") String role) {
        List<Member> result = memberService.getMemberByRole(role);
            return result;

    }

}
