package org.alina.agileengine.logiteam.services;

import org.alina.agileengine.logiteam.data.*;
import org.alina.agileengine.logiteam.resources.MemberResource;
import org.alina.agileengine.logiteam.validators.memberValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MemberService {

    @Autowired
    private MemberResource memberResource;

    public List<Member> updateMemberPhone(List<Member> memberList, String oldPhoneNumber, String newPhoneNumber) {

        memberValidator validator = new memberValidator();
        validator.catchEmptyList(memberList);
        validator.catchNullPointerException(oldPhoneNumber);

        for (Member member : memberList) {
            if (member.getPhoneNumber().equals(oldPhoneNumber)) {
                member.setPhoneNumber(newPhoneNumber);
            }
            memberList.add(member);
        };
        return memberList;
    }

    public List<Member> promoteMemberGrade(List<Member> list, String firstName, String lastName, EmployeeGrade newGrade) {

        for (Member member:list) {
            if (member.getFirstName().equals(firstName) && member.getLastName().equals(lastName)) {
                member.setGrade(newGrade);
            }
            list.add(member);
        }
        return list;
    }

    public List<Member> getMembers() {
        return memberResource.getMembers();
    }

    public List<Member> getMemberByRole(String role) {
        return memberResource.getMemberByRole(role);
    }

    public Member getMember(long id) {
        return memberResource.getMember(id);
    }

    public Member addMember(Member member) throws IOException {
        return memberResource.addMember(member);
    }

//    public boolean removeMember(long id) {
//        return memberResource.deleteMember(id);
//    }
}
