package org.alina.agileengine.logiteam.services;

import org.alina.agileengine.logiteam.data.*;
import org.alina.agileengine.logiteam.resources.MemberResource;
import org.alina.agileengine.logiteam.resources.ProjectResource;
import org.alina.agileengine.logiteam.validators.memberValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ProjectService {

    @Autowired
    private ProjectResource projectResource;

    List<Project> projectsList;

    public ProjectService() {
        Project project1 = new Project(000001, "LogiAnalytics", true, 30);

        Project project2 = new Project(000002, "TransVoyant", true, 52);

        Project project3 = new Project(000001, "ZoomData", true, 19);

        projectsList = new ArrayList<>();
        projectsList.add(project1);
        projectsList.add(project2);
        projectsList.add(project3);

    }

    public List<Project> getProjectlist() {
        return projectResource.getProjects();
    }

    public Project getProject(long id) {
        for (Project p:projectsList){
            if (p.getId() == id){
                return p;
            }
        }
        return null;
    }

    public List<Project> getProjectByName(String name) {
        List<Project> resultList = new ArrayList<>();

        for (Project project : projectsList) {
            if (project.getName().equals(name)) {
                resultList.add(project);
            }
        }
        return resultList;
    }

    public Project addProject(Project project) {
        return projectResource.addProject(project);
    }

    public boolean removeProject(long id) {
        return projectResource.removeProject(id);
    }
}
