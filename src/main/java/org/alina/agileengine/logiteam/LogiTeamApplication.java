package org.alina.agileengine.logiteam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.alina.agileengine.logiteam.services.MemberService;
import org.alina.agileengine.logiteam.services.TeamService;

import java.util.ArrayList;

@SpringBootApplication
public class LogiTeamApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogiTeamApplication.class, args);
	}

	{

		System.out.println("Hello Logi!");

		TeamService obj1 = new TeamService();

		MemberService obj2 = new MemberService();

		//System.out.println(obj1.createLogiTeam());

		//System.out.println(obj1.printTeam(TeamType.DRACO));

		System.out.println(obj2.updateMemberPhone(new ArrayList<>(), "+4567698", "+23434546743"));

		//System.out.println(obj2.promoteMemberGrade(new ArrayList<>(), "Bob", "Marley", EmployeeGrade.SENIOR));

	}

}
