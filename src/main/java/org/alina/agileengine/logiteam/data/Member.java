package org.alina.agileengine.logiteam.data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

public class Member extends Employee implements Serializable {

    @NotNull(message = "id parameter can not be null")
    private final long id;
    @NotNull(message = "role parameter can not be null")
    private MemberRole role;
    private boolean isTeamLead;
    private boolean isDeliveryManager;

    public Member(String firstName, String lastName, String sex, LocalDateTime dateOfBirth, EmployeePosition position, EmployeeGrade grade, double rate, long id, MemberRole role, boolean isTeamLead) {
        super(firstName, lastName, sex, dateOfBirth, position, grade, rate);
        this.id = id;
        this.role = role;
        this.isTeamLead = isTeamLead;
    }

    public boolean isTeamLead() {
        return isTeamLead;
    }

    public boolean isDeliveryManager() {
        return isDeliveryManager;
    }

    public void setTeamLead(boolean teamLead) {
        isTeamLead = teamLead;
    }

    public void setDeliveryManager(boolean deliveryManager) {
        isDeliveryManager = deliveryManager;
    }

    public MemberRole getRole() {
        return role;
    }

    public void setRole(MemberRole role) {
        this.role = role;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Member name: " + getFirstName() + " " + getLastName() + ", Member role: " + role + ", Team Lead: " + isTeamLead + ", Delivery Manager: " + isDeliveryManager;
    }
}