package org.alina.agileengine.logiteam.data;

import com.google.gson.annotations.SerializedName;

public enum MemberRole {
    @SerializedName("Developer")
    DEVELOPER("Developer"),
    @SerializedName("QA")
    QA("QA"),
    @SerializedName("ProductOwner")
    PRODUCTOWNER("ProductOwner"),
    @SerializedName("DevOps")
    DEVOPS("DevOps"),
    @SerializedName("Hr")
    HR("Hr");

    private String title;

    MemberRole(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
