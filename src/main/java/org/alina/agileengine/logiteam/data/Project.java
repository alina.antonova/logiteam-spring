package org.alina.agileengine.logiteam.data;

import javax.validation.constraints.NotNull;

public class Project {

    public Project(@NotNull(message = "id parameter can not be null") long id, String name, boolean isActive, int memberCount) {
        this.id = id;
        this.name = name;
        this.isActive = isActive;
        this.memberCount = memberCount;
    }

    @NotNull(message = "id parameter can not be null")
    private long id;
    private String name;
    private boolean isActive;
    private int memberCount;
    private float budget;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(int memberCount) {
        this.memberCount = memberCount;
    }

    public float getBudget() {
        return budget;
    }

    public void setBudget(float budget) {
        this.budget = budget;
    }

}
