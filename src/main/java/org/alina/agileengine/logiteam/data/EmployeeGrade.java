package org.alina.agileengine.logiteam.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EmployeeGrade {

    @JsonProperty("Junior")
    JUNIOR("Junior"),
    @JsonProperty("Middle")
    MIDDLE("Middle"),
    @JsonProperty("Senior")
    SENIOR("Senior");

    private String title;

    EmployeeGrade(String title) {
        this.title = title;
    }

    public String title() {
        return title;
    }
}
