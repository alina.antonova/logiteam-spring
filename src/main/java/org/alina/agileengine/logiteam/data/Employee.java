package org.alina.agileengine.logiteam.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

public class Employee extends Person implements Serializable {

    private EmployeePosition position;
    private EmployeeGrade grade;
    private LocalDateTime startDateInIt;
    private LocalDateTime startDateInAe;
    private double rate;

    public Employee(String firstName, String lastName, String sex, LocalDateTime dateOfBirth, EmployeePosition position, EmployeeGrade grade, double rate) {
        super(firstName, lastName, sex, dateOfBirth);
        this.position = position;
        this.grade = grade;
        this.rate = rate;
    }

    public EmployeePosition getPosition() {
        return position;
    }

    public void setPosition(EmployeePosition personPosition) {
        this.position = personPosition;
    }

    public EmployeeGrade getGrade() {
        return grade;
    }

    public void setGrade(EmployeeGrade grade) {
        this.grade = grade;
    }

    public LocalDateTime getStartDateInIt() {
        return startDateInIt;
    }

    public void setStartDateInIt(LocalDateTime startDateInIt) {
        this.startDateInIt = startDateInIt;
    }

    public LocalDateTime getStartDateInAe() {
        return startDateInAe;
    }

    public void setStartDateInAe(LocalDateTime startDateInAe) {
        this.startDateInAe = startDateInAe;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return "Position:: " + position + ", Grade: " + grade + ", Start date in IT: " + startDateInIt
                + ", Start date in AgileEngine: " + startDateInAe + ", Rate: " + rate;
    }

}
