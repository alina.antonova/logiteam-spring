package org.alina.agileengine.logiteam.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum TeamType {

    @JsonProperty("Draco")
    DRACO("Draco"),
    @JsonProperty("Aries")
    ARIES("Aries"),
    @JsonProperty("Columba")
    COLUMBA("Columba"),
    @JsonProperty("QA")
    QA("QA");

    private String title;

    TeamType(String title) {
        this.title = title;
    }

    public String title() {
        return title;
    }
}
