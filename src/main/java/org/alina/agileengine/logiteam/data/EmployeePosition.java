package org.alina.agileengine.logiteam.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public enum EmployeePosition {

    @JsonProperty("Java Developer")
    JAVADEVELOPER("Java Developer"),
    @JsonProperty("Js Developer")
    JSDEVELOPER("Js Developer"),
    @JsonProperty("Manual QA")
    MANUALQA("Manual QA"),
    @JsonProperty("Automation QA")
    AUTOMATIONQA("Automation QA");

    private String title;

    EmployeePosition(String title) {
        this.title = title;
    }

    public String title() {
        return title;
    }
}
