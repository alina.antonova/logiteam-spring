package org.alina.agileengine.logiteam.resources;

import com.google.gson.*;
import org.alina.agileengine.logiteam.data.Member;
import org.alina.agileengine.logiteam.jsonserializer.LocalDateTimeSerializer;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Repository;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class MemberResource {

    private Member member;
    final String PathToMembersList = "/Users/alinaantonova/Applications/logiteam-spring/src/main/resources/dataset/membersList.json";
    //final String PathToMembersList = "/Users/alina.antonova/Documents/logiteam-spring/src/main/resources/dataset/membersList";

    public List<Member> getMembers() {

        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(PathToMembersList))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray memberList = (JSONArray) obj;
            System.out.println(memberList);

            return memberList;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

//    public Member addMember(Member member) {
//        ObjectMapper objectMapper = new ObjectMapper();
//
//        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
//
//        ArrayNode root = null;
//        try {
//            root = (ArrayNode) objectMapper.readTree(new File(PathToMembersList));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        JsonNode obj = objectMapper.convertValue(member, JsonNode.class);
//        ArrayNode updatedRoot = root.add(obj);
//
//        try {
//            objectMapper.writeValue(new File(PathToMembersList), updatedRoot);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return member;
//    }

    public Member addMember(Member member) throws IOException {

        JsonParser jsonParser = new JsonParser();

        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer()).create();
        JsonArray array = new JsonArray();
        try {
            array = (JsonArray) jsonParser.parse(new FileReader(PathToMembersList));

        } catch (IOException | JsonParseException e) {
            e.printStackTrace();
        }

        JsonElement memberJson = gson.toJsonTree(member, Member.class);
        array.add(memberJson);

//        JsonObject obj = gson.toJson(member.toString(), JsonObject.class);
FileWriter fw =   new FileWriter(PathToMembersList);
fw.write(array.toString());
fw.close();
//        gson.toJson(array, fw);

        //jsonAdapter for enam


//
//
//        JsonObject node = gson.toJson(JsonObject.class, member.toString());
//        JsonArray updatedArray = array.add(member);
//
//
//        List<Member> memberList = new ArrayList<>();
//
//        for (int i = 0; i < array.size(); i++) {
//            JsonObject node = (JsonObject) array.get(i);
//            if (node.get("role").getAsString().equals(role)) {
//                Member member = gson.fromJson(node.toString(), Member.class);
//                memberList.add(member);
//            }
//        }
        return  member;
    }

//
//    public boolean deleteMember(long id) {
//        ObjectMapper objectMapper = new ObjectMapper();
//
//        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
//
//        try {
//            ArrayNode root = (ArrayNode) objectMapper.readTree(new File(PathToMembersList));
//
//        for (int i=0; i<=root.size() - 1; i++){
//            ObjectNode node = (ObjectNode) root.get(i);
//            if (node.get("id").canConvertToLong() && node.get("id").asLong() == id){
//                root.remove(i);
//                objectMapper.writeValue(new File(PathToMembersList), root);
//                return true;
//            }
//        }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return false;
//    }



    public Member getMember(long id) {

        JsonParser jsonParser = new JsonParser();

        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer()).create();
        JsonArray array = new JsonArray();
        try {
            array = (JsonArray) jsonParser.parse(new FileReader(PathToMembersList));

        } catch (IOException | JsonParseException e) {
            e.printStackTrace();
        }

        for (int i=0; i<=array.size(); i++){
            JsonObject node = (JsonObject) array.get(i);

            if (node.get("id").getAsLong() == id){
                    Member member = gson.fromJson(node.toString(), Member.class);
                    System.out.println(member);
                    return  member;
            }
        }
        return null;
    }



    public List<Member> getMemberByRole(String role) {

        JsonParser jsonParser = new JsonParser();

        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer()).create();
        JsonArray array = new JsonArray();
        try {
            array = (JsonArray) jsonParser.parse(new FileReader(PathToMembersList));

        } catch (IOException | JsonParseException e) {
            e.printStackTrace();
        }

        List<Member> memberList = new ArrayList<>();

        for (int i = 0; i < array.size(); i++) {
            JsonObject node = (JsonObject) array.get(i);
            if (node.get("role").getAsString().equals(role)) {
                Member member = gson.fromJson(node.toString(), Member.class);
                memberList.add(member);
            }
        }
        return  memberList;
    }

}
