package org.alina.agileengine.logiteam.resources;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.alina.agileengine.logiteam.data.Project;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Repository
public class ProjectResource {

    private Project project;
    final String PathToProjectsList = "/Users/alinaantonova/Applications/logiteam-spring/src/main/resources/dataset/projectsList";

    public List<Project> getProjects() {

        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(PathToProjectsList))
        {
            Object obj = jsonParser.parse(reader);

            JSONArray projectList = (JSONArray) obj;
            System.out.println(projectList);

            return projectList;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public Project addProject(Project project) {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        ArrayNode root = null;
        try {
            root = (ArrayNode) objectMapper.readTree(new File(PathToProjectsList));
        } catch (IOException e) {
            e.printStackTrace();
        }

        JsonNode obj = objectMapper.convertValue(project, JsonNode.class);
        ArrayNode updatedRoot = root.add(obj);

        try {
            objectMapper.writeValue(new File(PathToProjectsList), updatedRoot);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return project;
    }

    public boolean removeProject(long id) {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        try {
            ArrayNode root = (ArrayNode) objectMapper.readTree(new File(PathToProjectsList));

            for (int i=0; i<=root.size() - 1; i++){
                ObjectNode node = (ObjectNode) root.get(i);
                if (node.get("id").canConvertToLong() && node.get("id").asLong() == id){
                    root.remove(i);
                    objectMapper.writeValue(new File(PathToProjectsList), root);
                    return true;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

}
