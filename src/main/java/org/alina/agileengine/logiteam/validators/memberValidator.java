package org.alina.agileengine.logiteam.validators;

import org.alina.agileengine.logiteam.data.Member;
import java.util.List;

public class memberValidator {

    public void catchNullPointerException(String parameter) {
        if (parameter == null) {
            throw new NullPointerException(
                    "Parameter was null inside the method.");

        }
    }

    public void catchEmptyList(List<Member> memberList){

        boolean result = memberList.isEmpty();
        if (result)
            System.out.println("The 'memberList' is empty");
    }
}
